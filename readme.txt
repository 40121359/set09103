This repository is for the set09103 Advanced web technologies module.

Directories:
src - All code for module

src/labs - code for module practicals

src/coursework_1 - Web app for modules first coursework (There is another readme in here for app specific instructions)
src/coursework_1/lib - python libraries
src/coursework_1/templates - html files
src/coursework_1/licences - licenses for 3rd party libraries
src/coursework_1/static/img - image files for app
src/coursework_1/static/js - javascript files
src/coursework_1/static/stylesheets - css files
src/coursework_1/report - coursework report

src/coursework_2 - Web app for modules second coursework (There is another readme in here for app specific instructions)
src/coursework_2/etc - config file
src/coursework_2/lib - custom database handler
src/coursework_2/licences - licenses for 3rd party libraries
src/coursework_2/report - coursework report
src/coursework_2/static/img - image files for app
src/coursework_2/static/js - javascript files
src/coursework_2/static/css - css files
src/coursework_2/static/css - font (bootstrap)
src/coursework_2/templates - html template files
src/coursework_2/var - database and log file