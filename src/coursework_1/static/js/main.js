/* global jQuery */
/* global $ */

(function($){
	
	// Perform whenever page is loaded
	$(document).ready(function(){
				
		// Pagination
		var recs = $('#recs .rec').length;
		$('#pgesel').smartpaginator({ totalrecords: recs, recordsperpage: 10, datacontainer: 'recs', dataelement: '.rec', 
		initval: 0, next: 'Next', prev: 'Prev', first: 'First', last: 'Last', theme: 'black' });
			
	});
		
}(jQuery));

function typeFilter(className, filterid, selClassName){
	
	
	if($('#'+filterid).hasClass('typesel')){
		
		$('#'+filterid).removeClass('typesel');
		
		$('.'+className).addClass('hideable');
		
		
		if($('.selectedfilter').length == 0){
			$('.'+className).show();
			
			var recs = $('.hideable').length;
		
			$('#pgesel').empty();
			$('#pgesel').smartpaginator({ totalrecords: recs, recordsperpage: 10, datacontainer: 'recs', dataelement: '.hideable', 
			initval: 0, next: 'Next', prev: 'Prev', first: 'First', last: 'Last', theme: 'black' });
		}
		else{
			var selclass = $('.selectedfilter').attr('id');
			selclass = selclass.substring(0, selclass.length - 6);
			selclass = selclass.charAt(0).toUpperCase() + selclass.substr(1);
			
			
			
			$('.hideable').hide();
					
			$('.hideable.'+selclass).show();
			
			recs = $('.'+selclass).length;
			
			if (recs < 1){
				$('#pgesel').html("There are no results for "+className);
			}else{
				$('#pgesel').empty();
				$('#pgesel').smartpaginator({ totalrecords: recs, recordsperpage: 10, datacontainer: 'recs', dataelement: '.'+selclass, 
				initval: 0, next: 'Next', prev: 'Prev', first: 'First', last: 'Last', theme: 'black' });
			}
		}
	}else{
		if($('.selectedfilter').length == 0){
			
		
			$('.typesel').removeClass("typesel");
			
			$('#'+filterid).addClass("typesel");
			
			$('.'+selClassName).addClass('hideable');
			
			$('.'+className).hide();
			
			$('.'+className).removeClass('hideable');
			
			$('.hideable.'+selClassName).show();
			
			recs = $('.hideable.'+selClassName).length;
			
			if (recs < 1){
				$('#pgesel').html("There are no results for "+className);
			}else{
				$('#pgesel').empty();
				$('#pgesel').smartpaginator({ totalrecords: recs, recordsperpage: 10, datacontainer: 'recs', dataelement: '.'+selClassName, 
				initval: 0, next: 'Next', prev: 'Prev', first: 'First', last: 'Last', theme: 'black' });
			}
		}else{
			selclass = $('.selectedfilter').attr('id');
			selclass = selclass.substring(0, selclass.length - 6);
			selclass = selclass.charAt(0).toUpperCase() + selclass.substr(1);
			
			
			
			$('.typesel').removeClass("typesel");
			
			$('#'+filterid).addClass("typesel");
			
			$('.'+selClassName).addClass('hideable');
			
			$('.'+className).hide();
			
			$('.'+className).removeClass('hideable');
			
			$('.hideable.'+selclass).show();
			
			recs = $('.hideable.'+selclass).length;
			
			if (recs < 1){
				$('#pgesel').html("There are no results for "+className);
			}else{
				$('#pgesel').empty();
				$('#pgesel').smartpaginator({ totalrecords: recs, recordsperpage: 10, datacontainer: 'recs', dataelement: '.hideable.'+selclass, 
				initval: 0, next: 'Next', prev: 'Prev', first: 'First', last: 'Last', theme: 'black' });
			}
			
		}
	}
}

function filterClass(className, filterid){
	if ($('#'+filterid).hasClass("selectedfilter")){
					
		$('#'+filterid).removeClass("selectedfilter");
		
		$('.hideable').show();
		
		var recs = $('.hideable').length;
		
		$('#pgesel').empty();
		$('#pgesel').smartpaginator({ totalrecords: recs, recordsperpage: 10, datacontainer: 'recs', dataelement: '.hideable', 
		initval: 0, next: 'Next', prev: 'Prev', first: 'First', last: 'Last', theme: 'black' });
	}else{
		
		$('.selectedfilter').removeClass("selectedfilter");
		
		$('#'+filterid).addClass("selectedfilter");
		
		$('.hideable').hide();
					
		$('.hideable.'+className).show();
		
		recs = $('.hideable.'+className).length;
		
		if (recs < 1){
			$('#pgesel').html("There are no results for "+className);
		}else{
			$('#pgesel').empty();
			$('#pgesel').smartpaginator({ totalrecords: recs, recordsperpage: 10, datacontainer: 'recs', dataelement: '.'+className, 
			initval: 0, next: 'Next', prev: 'Prev', first: 'First', last: 'Last', theme: 'black' });
		}
	}
}


$(document).on('click','#actionfilter',function(){ filterClass('Action', 'actionfilter'); });

$(document).on('click','#adventurefilter',function(){ filterClass('Adventure','adventurefilter'); });

$(document).on('click','#animationfilter',function(){ filterClass('Animation','animationfilter'); });

$(document).on('click','#comedyfilter',function(){ filterClass('Comedy','comedyfilter'); });

$(document).on('click','#crimefilter',function(){ filterClass('Crime','crimefilter'); });

$(document).on('click','#documentaryfilter',function(){ filterClass('Documentary','documentaryfilter'); });

$(document).on('click','#familyfilter',function(){ filterClass('Family','familyfilter'); });

$(document).on('click','#fantasyfilter',function(){ filterClass('Fantasy','fantasyfilter'); });

$(document).on('click','#foreignfilter',function(){ filterClass('Foreign','foreignfilter'); });

$(document).on('click','#historyfilter',function(){ filterClass('History','historyfilter'); });

$(document).on('click','#horrorfilter',function(){ filterClass('Horror','horrorfilter'); });

$(document).on('click','#musicfilter',function(){ filterClass('Music','musicfilter'); });

$(document).on('click','#mysteryfilter',function(){ filterClass('Mystery','mysteryfilter'); });

$(document).on('click','#romancefilter',function(){ filterClass('Romance','romancefilter'); });

$(document).on('click','#sciencefilter',function(){ filterClass('Science','sciencefilter'); });

$(document).on('click','#tvmoviefilter',function(){ filterClass('TV','tvmoviefilter'); });

$(document).on('click','#thrillerfilter',function(){ filterClass('Thriller','thrillerfilter'); });

$(document).on('click','#warfilter',function(){ filterClass('War','warfilter'); });

$(document).on('click','#westernfilter',function(){ filterClass('Western','westernfilter'); });

$(document).on('click','#showfilter',function(){ typeFilter('movie','showfilter','show'); });

$(document).on('click','#moviefilter',function(){ typeFilter('show','moviefilter','movie'); });