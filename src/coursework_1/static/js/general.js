// Written by William James McGuigan

// Function to swap background image of search box
function fieldSwap(image){
	var sb = document.getElementById('searchbox');
	if(sb.value == ""){
	  sb.style.background = "url("+image+") no-repeat";
	}
}

// Function to swap button image
function buttonSwap(image){
	var sbtn = document.getElementById('searchButton');
	sbtn.src = image;
}