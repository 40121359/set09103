# Programmed by William James McGuigan 
# 40121359
# 12/10/2015

#importing online movie and tv show database
from lib.tmdb3 import *
set_key('fa04adc2b37160293bc5bdffec2d8e63')
set_cache(engine='file', filename='~/.tmdb3cache')

from lib.currencyconverter import currency_converter

# importing date for conversion of the way dates are displayed
from datetime import date

#import flask for web app
from flask import Flask, render_template, url_for, request, abort
app = Flask(__name__)

@app.route("/home", methods=['GET', 'POST'])
@app.route("/", methods=['GET', 'POST'])
def home():
	if request.method == 'POST':
		query = request.form['query']
		movies = searchMovie(query)
		shows = searchSeries(query)
		length = len(movies) + len(shows)
		return render_template('search.html', query=query, movies=movies, shows=shows, length=length)
	else:
		movies = Movie.mostpopular()
		tv = Series.mostpopular()
		return render_template('index.html', HomeSel='sel', movies=movies, tv=tv)

@app.route("/movies", methods=['GET', 'POST'])
def movies():
	movielist = request.args.get('list', '')
	genre = request.args.get('genre', '')
	if request.method == 'POST':
		query = request.form['query']
		movies = searchMovie(query)
		shows = searchSeries(query)
		length = len(movies) + len(shows)
		return render_template('search.html', query=query, movies=movies, shows=shows, length=length)
	else:
		if movielist == '':
			movies = Movie.mostpopular()[:50]
			return render_template('moviehome.html', MovSel='sel', popsel='sel', currentlist='popular', movies=movies)
		elif movielist == 'popular':
			movies = Movie.mostpopular()[:50]
			return render_template('moviehome.html', MovSel='sel', popsel='sel', currentlist='popular', movies=movies)
		elif movielist == 'toprated':
			movies = Movie.toprated()[:50]
			return render_template('moviehome.html', MovSel='sel', topsel='sel', currentlist='toprated', movies=movies)
		elif movielist == 'nowplaying':
			movies = Movie.nowplaying()[:50]
			return render_template('moviehome.html', MovSel='sel', nowsel='sel', currentlist='nowplaying', movies=movies)
		elif movielist == 'upcoming':
			movies = Movie.upcoming()[:50]
			return render_template('moviehome.html', MovSel='sel', upsel='sel', currentlist='upcoming', movies=movies)

@app.route("/shows", methods=['GET', 'POST'])
def shows():
	showlist = request.args.get('list', '')
	genre = request.args.get('genre', '')
	if request.method == 'POST':
		query = request.form['query']
		movies = searchMovie(query)
		shows = searchSeries(query)
		length = len(movies) + len(shows)
		return render_template('search.html', query=query, movies=movies, shows=shows, length=length)
	else:
		if showlist == '':
			shows = Series.mostpopular()[:50]
			return render_template('shows.html', TVSel='sel', popsel='sel', currentlist='popular', shows=shows)
		elif showlist == 'popular':
			shows = Series.mostpopular()[:50]
			return render_template('shows.html', TVSel='sel', popsel='sel', currentlist='popular', shows=shows)
		elif showlist == 'ontheair':
			shows = Series.ontheair()[:50]
			return render_template('shows.html', TVSel='sel', onairsel='sel', currentlist='ontheair', shows=shows)
		elif showlist == 'toprated':
			shows = Series.toprated()[:50]
			return render_template('shows.html', TVSel='sel', topsel='sel', currentlist='toprated', shows=shows)
		elif showlist == 'airingtoday':
			shows = Series.airingtoday()[:50]
			return render_template('shows.html', TVSel='sel', airtosel='sel', currentlist='airingtoday', shows=shows)

@app.route("/movie/<int:movid>", methods=['GET', 'POST'])
def movie(movid):
	if request.method == 'POST':
		query = request.form['query']
		movies = searchMovie(query)
		shows = searchSeries(query)
		length = len(movies) + len(shows)
		return render_template('search.html', query=query, movies=movies, shows=shows, length=length)
	else:
		mov = Movie(movid)
		release = mov.releasedate.strftime('%d/%m/%Y')
		cur = currency_converter.CurrencyConverter()
		if mov.budget == 0:
			budget = ""
			postbud = ""
		else:
			temp = cur.convert(mov.budget,'USD','GBP')
			if temp < 1000000:
				budget = round(temp, 0)
				postbud = ""
			else:
				budget = int(round(temp, -6)/1000000)
				postbud = "million"
		
		if mov.revenue == 0:
			revenue = ""
			postrev = ""
		else:
			temp = cur.convert(mov.revenue,'USD','GBP')
			if temp < 1000000:
				revenue = round(temp, 0)
				postrev = ""
			else:
				revenue = int(round(temp, -6)/1000000)
				postrev = "million"
		return render_template('movie.html', MovSel='sel', mov=mov, release=release, budget=budget, postbud=postbud, revenue=revenue, postrev=postrev)

@app.route("/series/<int:serid>", methods=['GET', 'POST'])
def series(serid):
	if request.method == 'POST':
		query = request.form['query']
		movies = searchMovie(query)
		shows = searchSeries(query)
		length = len(movies) + len(shows)
		return render_template('search.html', query=query, movies=movies, shows=shows, length=length)
	else:
		ser = Series(serid)
		airdate = ser.first_air_date.strftime('%d/%m/%Y')
		popularity = ("%.2f" % ser.popularity)
		return render_template('series.html', TVSel='sel', ser=ser, airdate=airdate, popularity=popularity)

@app.route("/series/<int:serid>/<int:seasonnum>", methods=['GET', 'POST'])
def season(serid, seasonnum):
	if request.method == 'POST':
		query = request.form['query']
		movies = searchMovie(query)
		shows = searchSeries(query)
		length = len(movies) + len(shows)
		return render_template('search.html', query=query, movies=movies, shows=shows, length=length)
	else:
		ser = Series(serid)
		sea = ser.seasons[seasonnum]
		return render_template('season.html', TVSel='sel', sea=sea, ser=ser)

@app.route("/series/<int:serid>/<int:seasonnum>/<int:epinum>", methods=['GET', 'POST'])
def episode(serid, seasonnum, epinum):
	if request.method == 'POST':
		query = request.form['query']
		movies = searchMovie(query)
		shows = searchSeries(query)
		length = len(movies) + len(shows)
		return render_template('search.html', query=query, movies=movies, shows=shows, length=length)
	else:
		ser = Series(serid)
		sea = ser.seasons[seasonnum]
		epi = sea.episodes[epinum]
		airdate = epi.air_date.strftime('%d/%m/%Y')
		return render_template('episode.html', TVSel='sel', epi=epi, ser=ser, airdate=airdate)
		
@app.errorhandler(404)
def page_not_found(error):
	return render_template('404.html')

@app.route('/force404')
def force404():
	abort(404)
		
# run app
if __name__ == "__main__":
	app.run(host='0.0.0.0',debug=True)
