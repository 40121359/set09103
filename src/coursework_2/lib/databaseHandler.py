# Programmed by William James McGuigan
# matric 40121359
# 21/11/2015

import sqlite3
from flask import g
import json
import base64
import bcrypt

db_location = 'var/banter.db'

# get database
def get_db():
	db = getattr(g, 'db', None)
	if db is None:
		db = sqlite3.connect(db_location)
		g.db = db
	return db

# close database connection
def close_db(exception):
	db = getattr(g, 'db', None)
	if db is not None:
		db.close

# initailise database
def init_db(app):
	with app.app_context():
		db = get_db()
		with app.open_resource('schema.sql', mode='r') as f:
			db.cursor().executescript(f.read())
		db.commit()

# gets all user in order of popularity
def get_all_users():
	db = get_db()
	sql = "SELECT username, profilepic FROM users ORDER BY visited DESC LIMIT 50;"
	response = []
	for user in db.cursor().execute(sql):
		username = user[0]
		profilepic = base64.b64encode(user[1])
		response.append({'username' : username, 'profilepic': profilepic})
	return json.dumps(response)

# get all posts from user
def get_user_posts(username):
	db = get_db()
	sql = "SELECT * FROM posts WHERE username = '"+username+"' ORDER BY post_time DESC;"
	response = []
	for post in db.cursor().execute(sql):
		postid = post[0]
		un = post[1]
		upost = post[2]
		upost_time = post[3]
		sql = "SELECT profilepic FROM users WHERE username='"+username+"';"
		for usr in db.cursor().execute(sql):
			pic = base64.b64encode(usr[0])
		response.append({'postid' : postid, 'username' : un, 'post' : upost, 'post_time' : upost_time, 'pic' : pic})
	return json.dumps(response)

# get profile pic of user
def get_pic(username):
	db = get_db()
	sql = "SELECT profilepic FROM users WHERE username='"+username+"';"
	for usr in db.cursor().execute(sql):
		pic = base64.b64encode(usr[0])
	return pic
	
# get a users feed
def get_user_feed(username):
	db = get_db()
	sql = "SELECT follow.*, posts.* FROM posts INNER JOIN follow ON posts.username=follow.following_user WHERE follow.follower='"+username+"' ORDER BY posts.post_time DESC;"
	response = []
	for post in db.cursor().execute(sql):
		following_user = post[1]
		uname = post[0]
		postid = post[2]
		ps = post[4]
		ps_time = post[5]
		pic = get_pic(post[1])
		response.append({'postid' : postid, 'following' : following_user, 'username' : uname, 'post' : ps, 'post_time' : ps_time, 'pic' : pic })
	return json.dumps(response)

# Return all data for user
def get_usr_data(username):
	db = get_db()
	sql = "SELECT * FROM users WHERE username='"+username+"';"
	user = []
	for row in db.cursor().execute(sql):
		user.append(row[0])
		user.append(row[1])
		user.append(row[2])
		user.append(row[3])
		user.append(row[4])
		user.append(base64.b64encode(row[5]))
	return user

# add a post to database
def add_post(username, post):
	db = get_db()
	sql = "INSERT INTO posts (username,post,post_time) VALUES ('"+username+"', '"+post+"', CURRENT_TIMESTAMP);"
	db.cursor().execute(sql)
	db.commit()
	sql = "SELECT postid FROM posts WHERE username='"+username+"' and post='"+post+"' ORDER BY post_time DESC;"
	for row in db.cursor().execute(sql):
		return row[0]

# add user to database
def add_user(username, pswdhash, fullname, email):
	db = get_db()
	sql = "INSERT INTO users (username, pswdhash, fullname, visited, email) VALUES ('"+username+"', '"+pswdhash+"', '"+fullname+"', 1, '"+email+"');"
	db.cursor().execute(sql)
	db.commit()

# add picture to database
def add_picture(picture, username):
	db = get_db()
	with open(picture, 'rb') as pic:
		blob = pic.read()
		sql = "UPDATE users set profilepic=? WHERE username='"+username+"';"
		db.cursor().execute(sql, [buffer(blob)])
		db.commit()

# add a follow
def follow_user(username, to_follow):
	db = get_db()
	sql = "INSERT INTO follow (follower, following_user) VALUES ('"+username+"', '"+to_follow+"');"
	db.cursor().execute(sql)
	db.commit()

# check password
def verify_usr(username, pswdhash):
	db = get_db()
	sql = "SELECT pswdhash FROM users WHERE username='"+username+"';"
	try:
		for res in db.cursor().execute(sql):
			pswd = res[0]
		if pswd != None:
			if pswd == bcrypt.hashpw(pswdhash.encode('utf-8'), pswd.encode('utf-8')):
				return True
		else:
			return False
	except:
		return False

# Check if user is following another
def check_follow(username, follow):
	db = get_db()
	sql = "SELECT * FROM follow WHERE follower='"+username+"' AND following_user='"+follow+"';"
	try:
		for res in db.cursor().execute(sql):
			if res[0]:
				return True
			else:
				return False
	except:
		pass
	return False

# check user exists
def usr_exists(username):
	db = get_db()
	sql = "SELECT * FROM users WHERE username='"+username+"';"
	try:
		for usr in db.cursor().execute(sql):
			if usr[0]:
				return True
			else:
				return False
	except:
		pass
	return False

# Adds one to the view count of specified user
def add_pge_view(username):
	db = get_db()
	sql = "UPDATE users SET visited = visited + 1 WHERE username='"+username+"';"
	db.cursor().execute(sql)
	db.commit()

# remove user
def remove_usr(username):
	db = get_db()
	db.cursor().execute("DELETE FROM users WHERE username='"+username+"';")
	db.cursor().execute("DELETE FROM posts WHERE username='"+username+"';")
	db.cursor().execute("DELETE FROM follow WHERE follower='"+username+"';")
	db.cursor().execute("DELETE FROM follow WHERE following_user='"+username+"';")
	db.commit()

#remove post
def remove_post(postid):
	db = get_db()
	db.cursor().execute("DELETE FROM posts WHERE postid="+postid+";")
	db.commit()

# unfollow user
def unfollow_user(username, unfollow):
	db = get_db()
	db.cursor().execute("DELETE FROM follow WHERE follower='"+username+"' AND following_user='"+unfollow+"';")
	db.commit()