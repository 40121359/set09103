DROP TABLE if EXISTS users;
DROP TABLE if EXISTS posts;
DROP TABLE if EXISTS follow;

CREATE TABLE users (
	username text PRIMARY KEY,
	pswdhash text NOT NULL,
	fullname text NOT NULL,
	visited INTEGER NOT NULL,
	email text UNIQUE NOT NULL,
	profilepic BLOB);

CREATE TABLE posts(
	postid INTEGER PRIMARY KEY AUTOINCREMENT,
	username text NOT NULL,
	post text NOT NULL,
	post_time text NOT NULL);

CREATE TABLE follow(
	follower text NOT NULL,
	following_user text NOT NULL);