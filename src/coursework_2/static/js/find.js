/* global $ */
$(function(){
	var $container = $('#userlist');
	
	$.ajax({
		method:"GET",
		url:"/api/allusers",
		dataType:"JSON",
		success: function(users){
			$.each(users, function(i, user){
				$container.append('<a href="/profile/'+
				user.username+'"><div class="user infobox"><img class="userpic" src="data:image/png;base64,'+
				user.profilepic+'"><p class="username">'+
				user.username+'</p></a>');
			});
		},
		error: function(){
			alert('Error loading users!');
		}
		
	});
	
});