/* global $ */
$(function(){
	
	// Variables
	var $container = $('#postfeed');
	var $username = $('#username');
	var $postinput = $('#inputBanter');
	var $propic = 0;
	
	// Creates post string
	function makePost(post){
		var newPost = '<div class="post infobox"><button id="'+
				post.postid+'" class="btn btn-primary remove">Remove</button><a href="/profile/'+
				post.username+'"><img class="postpic" src="data:image/png;base64,'+
				post.pic+'"></a><p class="following">'+
				post.username+'</p><p class="posttext">'+
				post.post+'</p></div>';
		
		return newPost;
	}
	
	// Add a new post div to post feed
	function addPost(post , append){
		if(append == true){
			$container.append(makePost(post));
			$propic = post.pic;
		}
		else{
			$container.prepend(makePost(post));
			$propic = post.pic;
		}
	}
	
	// Get posts from database using ajax
	$.ajax({
		method:"GET",
		url:"/api/"+$username.text(),
		dataType:"JSON",
		success: function(posts){
			$.each(posts, function(i, post){
				addPost(post, true);
			});
		},
		error: function(){
			alert('Error loading feed!');
		}
	});
	
	// post to database when button is clicked
	$('#banterbtn').on('click', function(){
		
		if($.trim($("#inputBanter").val())){
			
			var postData = [{
				"username": $username.text(),
				"post":$postinput.val(),
				"pic":$propic,
			}];
			
			$.ajax({
				type:"POST",
				url:"/api/post/",
				data:JSON.stringify({ Post : postData }),
				dataType: "JSON",
				contentType: "application/json; charset=utf-8",
				success: function(newPost){
					addPost(newPost, false);
				},
				error: function(){
					alert("Error adding post!");
				}
			});
		}
		else{
			alert("You have to type something to banter!");
		}
		
	});
	
	// Delete selected post from database
	$container.delegate('.post .remove','click', function(){
		
		var $div = $(this).closest('div');
		
		$.ajax({
			type:"DELETE",
			url:"/api/post/"+$(this).attr('id'),
			success: function(){
				$div.fadeOut(500, function(){
					$(this).remove();
				});
			},
			error: function(){
				alert("Error deleting post!");
			}
		});
	});
});