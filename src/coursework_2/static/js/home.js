/* global $ */
$(function(){
	var $container = $('#postfeed');
	
	var $username = $('#username').text();
	
	$.ajax({
		method:"GET",
		url:"/api/feed/"+$username,
		dataType:"JSON",
		success: function(posts){
			$.each(posts, function(i, post){
				$container.append('<a href="/profile/'+
				post.username+'"><div class="post infobox"><img class="postpic" src="data:image/png;base64,'+
				post.pic+'"><p class="following">'+
				post.following+'</p><p class="posttext">'+
				post.post+'</p></div></a>');
			});
		},
		error: function(){
			alert('Error loading feed!');
		}
	});
});