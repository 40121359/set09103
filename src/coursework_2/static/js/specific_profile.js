/* global $ */
$(function(){
	
	// Variables
	var $container = $('#postfeed');
	var $username = $('#username');
	var $propic = 0;
	
	// Creates post string
	function makePost(post){
		var newPost = '<div class="post infobox"><img class="postpic" src="data:image/png;base64,'+
				post.pic+'"><p class="following">'+
				post.username+'</p><p class="posttext">'+
				post.post+'</p></div>';
		
		return newPost;
	}
	
	// Add a new post div to post feed
	function addPost(post , append){
		if(append == true){
			$container.append(makePost(post));
			$propic = post.pic;
		}
		else{
			$container.prepend(makePost(post));
			$propic = post.pic;
		}
	}
	
	// Get posts from database using ajax
	$.ajax({
		method:"GET",
		url:"/api/"+$username.text(),
		dataType:"JSON",
		success: function(posts){
			$.each(posts, function(i, post){
				addPost(post, true);
			});
		},
		error: function(){
			alert('Error loading feed!');
		}
	});
	
	// Follow or unfollow the current user
	$('#followbtn').on('click', function(){
		if($('#followbtn').hasClass('btn-primary')){
			var postData = [{
				"user": $username.text(),
			}];
			
			$.ajax({
				type:"POST",
				url:"/api/follow",
				data:JSON.stringify({ Follow : postData }),
				dataType: "JSON",
				contentType: "application/json; charset=utf-8",
				success: function(newFollow){
					$('#followbtn').html('Unfollow!');
					$('#followbtn').removeClass('btn-primary');
					$('#followbtn').addClass('btn-success');
				},
				error: function(){
					alert("There was an error following "+$username.text()+"!");
				}
			});
		}
		else{
			$.ajax({
				type:"DELETE",
				url:"/api/unfollow/"+$username.text(),
				success: function(){
					$('#followbtn').html('Follow!');
					$('#followbtn').removeClass('btn-success');
					$('#followbtn').addClass('btn-primary');
				},
				error: function(){
					alert("There was an error unfollowing "+$username.text()+"!");
				}
			});
		}
	});
});