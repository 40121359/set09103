# Programmed by William James McGuigan
# 40121359
# 21/11/2015

# import required libraries
import sqlite3
import json
import ConfigParser
import bcrypt
import logging
import os

# import external code
from lib.databaseHandler import *

# import flask
from flask import Flask, render_template, request, redirect, url_for, flash, session, jsonify, abort
app = Flask(__name__)

# import file handler for logging
from logging.handlers import RotatingFileHandler

def init(app):
	config = ConfigParser.ConfigParser()
	try:
		config_location = "etc/defaults.cfg"
		config.read(config_location)
		
		app.config['DEBUG'] = config.get("config", "debug")
		app.config['ip_address'] = config.get("config","ip_address")
		app.config['port'] = config.get("config","port")
		app.config['url'] = config.get("config","url")
		app.secret_key = config.get("config","secret_key")
		
		app.config['log_file'] = config.get("logging", "name")
		app.config['log_location'] = config.get("logging", "location")
		app.config['log_level'] = config.get("logging", "level")
	except:
		print "Could not read configs from: ", config_location

def logs(app):
	log_pathname = app.config['log_location'] + app.config['log_file']
	file_handler = RotatingFileHandler(log_pathname, maxBytes=1024*1024*10, backupCount=1024)
	formatter = logging.Formatter("%(levelname)s | %(asctime)s | %(module)s | %(funcName)s | %(message)s")
	file_handler.setFormatter(formatter)
	app.logger.setLevel( app.config['log_level'] )
	app.logger.addHandler(file_handler)

def get_app():
	return app

def encrypt_pswd(pswd):
	return bcrypt.hashpw(pswd.encode('utf-8'), bcrypt.gensalt())

@app.teardown_appcontext
def close_db_connection(exception):
	close_db(exception)

@app.route('/api/<uname>/')
def user_posts(uname):
	return get_user_posts(uname)

@app.route('/api/allusers/')
def all_users():
	return get_all_users()

@app.route('/api/feed/<uname>/')
def user_feed(uname):
	return get_user_feed(uname)

@app.route('/api/post/', methods=['POST'])
def post():
	try:
		if(session['username']):
			data = request.json
			data["Post"][0]["postid"] = add_post(data["Post"][0]["username"], data["Post"][0]["post"])
			return jsonify(data["Post"][0])
	except KeyError:
		pass
	flash("You have to be logged on to post!")
	return redirect(url_for('root'))

@app.route('/api/post/<int:postid>', methods=['DELETE'])
def rm_post(postid):
	try:
		if(session['username']):
			remove_post(str(postid))
			return str(postid)
	except KeyError:
		pass
	flash("You have to be logged on to delete a post")
	return redirect(url_for('root'))

@app.route('/api/follow', methods=['POST'])
def follow():
	data = request.json
	follow_user(session['username'], data["Follow"][0]["user"])
	return jsonify(data["Follow"][0])

@app.route('/api/unfollow/<unfollow>', methods=['DELETE'])
def unfollow(unfollow):
	unfollow_user(session['username'], unfollow)
	return str(unfollow)

@app.route('/', methods=['POST','GET'])
def root():
	if request.method == 'POST':
		username = request.form['username']
		if verify_usr(username, request.form['password']) == True:
			session['username'] = username
			app.logger.info(username+" has logged in")
			return redirect(url_for('home'))
		else:
			flash("Username or password is invalid!")
			return redirect(url_for('root'))
	else:
		return render_template('index.html')

@app.route('/about')
def about():
	return render_template('about.html')

@app.route('/signup', methods=['POST','GET'])
def signup():
	if request.method == 'POST':
		fullname = request.form['fullname']
		email = request.form['email']
		username = request.form['username']
		pswd = encrypt_pswd(request.form['password'])
		try:
			add_user(username, pswd, fullname, email)
			add_picture("static/img/DefaultProfilePic.png", username)
			add_post(username, "This is your first post!")
			firstname = str(fullname).split()
			flash(str("Congratulations "+firstname[0]+"! You can now log in as "+username))
			return redirect(url_for('root'))
		except:
			pass
		flash("The user "+username+" already exists")
		return redirect(url_for('signup'))
	else:
		return render_template('signup.html')

@app.route('/home')
def home():
	try:
		if(session['username']):
			user = get_usr_data(session['username'])
			username = user[0]
			fullname = user[2]
			email = user[4]
			profilepic = user[5]
			return render_template('home.html', username=username, fullname=fullname, email=email, profilepic=profilepic)
	except KeyError:
		pass
	flash("You have to be logged in to access that page!")
	return redirect(url_for('root'))

@app.route('/profile/', methods=['POST', 'GET'])
def profile():
	if request.method == 'POST':
		try:
			f = request.files['picture']
			username = session['username']
			f.save('incoming/'+username+'.png')
			add_picture('incoming/'+username+'.png', username)
			os.remove('incoming/'+username+'.png')
			flash("Picture uploaded")
			return redirect(url_for('profile'))
		except:
			pass
		flash("There was an error uploading picture")
		return redirect(url_for('profile'))
	else:
		try:
			if(session['username']):
				user = get_usr_data(session['username'])
				username = user[0]
				fullname = user[2]
				email = user[4]
				profilepic = user[5]
				return render_template('profile.html', username=username, fullname=fullname, email=email, profilepic=profilepic)
		except KeyError:
			pass
		flash("You have to be logged in to access that page!")
		return redirect(url_for('root'))

@app.route('/profile/<username>')
def specific_profile(username):
	if usr_exists(username) == True:
		user = get_usr_data(username)
		fullname = user[2]
		email = user[4]
		profilepic = user[5]
		try:
			if(session['username']):
				loggedin = session['username']
				following = check_follow(loggedin, username)
				add_pge_view(username)
				return render_template('specific_profile.html', username=username, fullname=fullname, email=email, profilepic=profilepic, loggedin=loggedin, following=following)
		except KeyError:
			pass
		add_pge_view(username)
		return render_template('specific_profile.html', username=username, fullname=fullname, email=email, profilepic=profilepic)
	else:
		flash("That user doesn't exist")
		try:
			if(session['username']):
				return redirect(url_for('home'))
		except KeyError:
			pass
		return redirect(url_for('root'))

@app.route('/find', methods=['POST', 'GET'])
def find():
	if request.method == 'POST':
		searchQuery = request.form['query']
		if usr_exists(searchQuery) == True:
			return redirect(url_for('specific_profile', username=searchQuery))
		else:
			flash("That user does't exist")
			return redirect(url_for('find'))
	else:
		return render_template('find.html')

@app.route('/signout/')
def signout():
	app.logger.info(session['username']+" has logged out")
	session.pop('username', None)
	flash("You have logged out!")
	return redirect(url_for('root'))

@app.errorhandler(404)
def page_not_found(error):
	flash("404 error: That page doesn't exist!")
	try:
		if(session['username']):
			return redirect(url_for('profile'))
	except KeyError:
		pass
	return redirect(url_for('root'))

@app.route('/force404')
def force404():
	abort(404)

if __name__ == "__main__":
	init(app)
	logs(app)
	app.run(
		host=app.config['ip_address'], 
		port=int(app.config['port']))