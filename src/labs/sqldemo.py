from flask import Flask, g
import sqlite3
import json

app = Flask(__name__)
db_location = 'var/test.db'

def get_db():
	db = getattr(g, 'db', None)
	if db is None:
		db = sqlite3.connect(db_location)
		g.db = db
	return db

@app.teardown_appcontext
def close_db_connection(exception):
	db = getattr(g, 'db', None)
	if db is not None:
		db.close

def init_db():
	with app.app_context():
		db = get_db()
		with app.open_resource('schema.sql', mode='r') as f:
			db.cursor().executescript(f.read())
		db.commit()

@app.route("/")
def root():
	db = get_db()
	db.cursor().execute('INSERT INTO albums VALUES ("American Beauty", "Grateful Dead", "CD")')
	db.commit()
	
	response = []
	#page = []
	#page.append('<html><ul>')
	sql = "SELECT rowid, * FROM albums ORDER BY artist"
	for row in db.cursor().execute(sql):
		rowid = row[0]
		title = row[1]
		artist = row[2]
		mediatype = row[3]
		response.append({'rowid': rowid, 'title' : title, 'artist' : artist, 'mediatype' : mediatype})
	
	#page.append('</ul></html>')
	return json.dumps(response)

@app.route("/init")
def dbinitroute():
	init_db()
	return "init"

if __name__ == "__main__":
	app.run(host="0.0.0.0", debug=True)